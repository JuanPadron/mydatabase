﻿using database.DataSet1TableAdapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace database
{
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        public string name;
        public string password;
        public UsersTableAdapter us = new UsersTableAdapter();
        public DataSet1 dt = new DataSet1();
        private void txtName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = dt.Users;
            us.Fill(dt.Users);
        }
        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            var query = from user in dt.Users
                        where (user.UserName == txtName.Text)
                        where (user.UserPassword == txtPassword.Text)
                        select user;

            if (query.Count() > 0)
            {
                MainWindow mainW = new MainWindow();
                LoginWindow logW = new LoginWindow();
                mainW.Show();
                logW.Close();
            }
            else
            {
                MessageBox.Show("User does not exist");
            }
        }
        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            DataSet1.UsersRow row = (DataSet1.UsersRow)dt.Users.NewRow();
            row.UserName = txtName.Text;
            row.UserPassword = txtPassword.Text;
            dt.Users.AddUsersRow(row);
            us.Update(dt);
            MessageBox.Show("User created");
        }
    }
}
